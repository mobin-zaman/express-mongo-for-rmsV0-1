
var express = require('express');

var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var path = require('path');

require('dotenv').config();

var mapsRouter = require('./routes/maps');

var app = express();

// view engine setup

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// app.use('/', mapsRouter);

app.use('/map', mapsRouter)
app.use(express.static(path.join(__dirname, 'public')));

// Define route for the homepage
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/test', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'example-with-boundaries.html'));
})
// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//add port
const PORT = process.env.PORT || 3000;

// //run on PORT
app.listen(PORT, () => { console.log(`Server is running on port ${PORT}`) })
module.exports = app;

// const nanoid = require('nanoid');
const getDb = require('../db/connection');
const uuid = require('uuid');
let db;
(async () => {
    db = await getDb();
})();

const mapCollection = () => db.collection('mapsCoordinates');
const deploymentCollection = () => db.collection('deployments');

const generateId = () => uuid.v4();

// const initiateMapInstance = async (instanceId, deploymentCode) => {
    const initiateMapInstance = async (instanceId, lat, lng) => {
        // Use upsert with $setOnInsert to insert only if the document doesn't exist
        return await mapCollection().updateOne(
            { instanceId: instanceId },
            {
                $set: {
                    lat: lat,
                    lng: lng
                },
                $setOnInsert: {
                    // Fields to set only when inserting a new document
                    // Add additional fields if needed
                    instanceId: instanceId,
                }
            },
            { upsert: true }
        );
    };
    
const getInstanceId = async () => {
    return generateId();
}

const getDeploymentCoordinates = async (deploymentCode) => {
    console.log("🚀 ~ getDeploymentCoordinates ~ deploymentCode:", deploymentCode)
    const deployment = await deploymentCollection().findOne({ code: deploymentCode });
    console.log("🚀 ~ getDeploymentCoordinates ~ deployment:", deployment)
    return deployment.centerCoordinate;
}

// const updateMapInstance = async (instanceId, lat, lng) => {
//     const result= await mapCollection().updateOne(
//         { instanceId: instanceId },
//         {
//             $set: {
//                 lat: lat,
//                 lng: lng
//             }
//         }
//     );
//     console.log("��� ~ updateMapInstance ~ result:", result);
    
// };



module.exports = {
    getInstanceId,
    getDeploymentCoordinates,
    // updateMapInstance,
    initiateMapInstance
}

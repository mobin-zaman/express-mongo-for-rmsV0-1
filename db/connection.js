// import { MongoClient } from "mongodb";
const mongodb = require('mongodb');

async function getDb() {
const connectionString = process.env.ATLAS_URI || "";

const client = new mongodb.MongoClient(connectionString);

let conn;
try {
  conn = await client.connect();
} catch(e) {
  console.error(e);
}

let db = conn.db(process.env.ATLAS_DB);

return db
}

module.exports = getDb;
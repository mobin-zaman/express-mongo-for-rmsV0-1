const express = require('express');
const getDb = require('../db/connection');
const mapService = require('../service/map');
const router = express.Router();

router.post('/map-session-initiate', async (req, res, next) => {

    const {  incidentId, } = req.body;

    await mapService.initiateMapInstance(incidentId, );
    res.send(200);

})

router.put('/map-session-update', async (req, res, next) => {

    const {  incidentId, lat, lng} = req.body;

    await mapService.initiateMapInstance(incidentId, lat, lng);
    res.sendStatus(200);
})

router.get('/map-session-get-lat-lng/:incidentId', async (req, res, next) => {
    
    const { incidentId } = req.params;
    const mapInstance = await mapService.getMapInstance(incidentId);
    res.sendStatus(mapInstance);
})

router.get('/deployment-coordinates/:deploymentCode', async (req, res, next) => {
    
    const { deploymentCode } = req.params;
    const coordinates = await mapService.getDeploymentCoordinates(deploymentCode);
    res.send(coordinates);
})


router.get('/', async (req, res, next) => {
	res.send({msg:"SERVICE RUNNING"})
})

module.exports = router;
